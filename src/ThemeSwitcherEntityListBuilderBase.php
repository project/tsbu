<?php

namespace Drupal\tsbu;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for entity list builders for theme switcher entities.
 */
abstract class ThemeSwitcherEntityListBuilderBase extends EntityListBuilder {

  /**
   * Theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected ThemeHandlerInterface $themeHandler;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructor for SelectThemeListBuilder object.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ThemeHandlerInterface $theme_handler, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);
    $this->themeHandler = $theme_handler;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('theme_handler'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Changed');
    $header['uid'] = $this->t('User');
    $header['theme'] = $this->t('Theme');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\tsbu\SelectThemeInterface $entity */
    $row['created'] = $entity->get('created')->value ? $this->dateFormatter->format($entity->get('created')->value) : '';
    $row['changed'] = $entity->get('changed')->value ? $this->dateFormatter->format($entity->get('changed')->value) : '';
    $row['uid']['data'] = [
      '#theme' => 'username',
      '#account' => $entity->getOwner(),
    ];
    $row['theme'] = $this->themeHandler->getName($entity->getTheme());
    return $row + parent::buildRow($entity);
  }

}
