<?php

namespace Drupal\tsbu;

use Drupal\Core\Session\AccountInterface;

/**
 * Interface for select theme storage.
 */
interface SelectThemeStorageInterface {

  /**
   * The method that return select theme by user.
   */
  public function loadByUser(AccountInterface $user): ?SelectThemeInterface;

  /**
   * The method checks if the user has already selected the theme.
   */
  public function existsByUser(int $uid): bool;

}
