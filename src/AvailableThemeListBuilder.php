<?php

namespace Drupal\tsbu;

/**
 * Provides a list controller for the available theme entity type.
 */
class AvailableThemeListBuilder extends ThemeSwitcherEntityListBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total available themes: @total', ['@total' => $total]);
    return $build;
  }

}
