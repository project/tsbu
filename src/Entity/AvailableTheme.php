<?php

namespace Drupal\tsbu\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\tsbu\AvailableThemeInterface;
use Drupal\tsbu\EntityThemeTrait;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the available theme entity class.
 *
 * @ContentEntityType(
 *   id = "available_theme",
 *   label = @Translation("Available theme"),
 *   label_collection = @Translation("Available themes"),
 *   label_singular = @Translation("available theme"),
 *   label_plural = @Translation("available themes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count available theme",
 *     plural = "@count available themes",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\tsbu\AvailableThemeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\tsbu\AvailableThemeAccessControlHandler",
 *     "storage" = "Drupal\tsbu\AvailableThemeStorage",
 *     "form" = {
 *       "add" = "Drupal\tsbu\AvailableThemeForm",
 *       "edit" = "Drupal\tsbu\AvailableThemeForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "available_theme",
 *   admin_permission = "administer available theme",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/appearance/tsbu/available-theme",
 *     "add-form" = "/available-theme/add",
 *     "edit-form" = "/available-theme/{available_theme}/edit",
 *     "delete-form" = "/available-theme/{available_theme}/delete",
 *   },
 *   constraints = {
 *     "UniqueAvailableThemeConstraint" = {}
 *   },
 * )
 */
class AvailableTheme extends ContentEntityBase implements AvailableThemeInterface {

  use EntityOwnerTrait;
  use EntityThemeTrait;
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::themeBaseFieldDefinitions();

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the available theme was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the available theme was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ]);
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setSetting('target_type', 'user')
      ->setRequired(TRUE)
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setReadOnly(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    if ($rel == 'canonical') {
      return Url::fromRoute('entity.available_theme.collection', [], $options);
    }
    return parent::toUrl($rel, $options);
  }

}
