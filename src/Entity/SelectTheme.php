<?php

namespace Drupal\tsbu\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\tsbu\EntityThemeTrait;
use Drupal\tsbu\SelectThemeInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the select theme entity class.
 *
 * @ContentEntityType(
 *   id = "select_theme",
 *   label = @Translation("Select Theme"),
 *   label_collection = @Translation("Select Themes"),
 *   label_singular = @Translation("select theme"),
 *   label_plural = @Translation("select themes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count select theme",
 *     plural = "@count select themes",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\tsbu\SelectThemeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\tsbu\SelectThemeAccessControlHandler",
 *     "storage" = "Drupal\tsbu\SelectThemeStorage",
 *     "form" = {
 *       "default" = "Drupal\tsbu\SelectThemeForm",
 *       "add" = "Drupal\tsbu\SelectThemeForm",
 *       "edit" = "Drupal\tsbu\SelectThemeForm",
 *       "switch" = "Drupal\tsbu\Form\SelectThemeForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "select_theme",
 *   admin_permission = "administer select theme",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/appearance/tsbu/select-theme",
 *     "add-form" = "/select-theme/add",
 *     "edit-form" = "/select-theme/{select_theme}/edit",
 *     "delete-form" = "/select-theme/{select_theme}/delete",
 *   },
 * )
 */
class SelectTheme extends ContentEntityBase implements SelectThemeInterface {

  use EntityOwnerTrait;
  use EntityThemeTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::themeBaseFieldDefinitions();

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setSetting('target_type', 'user')
      ->setRequired(TRUE)
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->addConstraint('UniqueField')
      ->setReadOnly(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the select theme was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the select theme was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    if ($rel == 'canonical') {
      return Url::fromRoute('entity.available_theme.collection', [], $options);
    }
    return parent::toUrl($rel, $options);
  }

}
