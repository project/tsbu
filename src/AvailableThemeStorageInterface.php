<?php

namespace Drupal\tsbu;

use Drupal\Core\Session\AccountInterface;

/**
 * Interface for available storage.
 */
interface AvailableThemeStorageInterface {

  /**
   * The that return available theme by user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user.
   *
   * @return array
   *   The available themes.
   */
  public function getAvailableThemesByUser(AccountInterface $user): array;

  /**
   * Check if available theme exists by theme.
   *
   * @param string $theme
   *   The theme machine name.
   *
   * @return bool
   *   Return true if exists.
   */
  public function existsByTheme(string $theme): bool;

  /**
   * Check if available theme exists by theme and user.
   *
   * @param string $theme
   *   The theme machine name.
   * @param int $uid
   *   The user id.
   *
   * @return bool
   *   Return true if exists.
   */
  public function existsByThemeAndUser(string $theme, int $uid): bool;

}
