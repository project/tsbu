<?php

namespace Drupal\tsbu\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Theme switcher settings form.
 */
class ThemeSwitcherSettingsForm extends ConfigFormBase {

  /**
   * Config name.
   */
  const SETTINGS_NAME = 'tsbu.theme_switcher_settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tsbu_theme_switcher_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS_NAME);
    $form['exclude_paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exclude page paths'),
      '#description' => $this->t('Specify the paths that will be ignored and will use the default theme or the theme of another user specified in the route parameters if this functionality is enabled. Enter each path on a different line. Wildcards (*) can be used.'),
      '#default_value' => $config->get('exclude_paths') ? implode(PHP_EOL, $config->get('exclude_paths')) : [],
    ];
    $form['enable_theme_by_user_from_route'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable theme by user from route'),
      '#description' => $this->t('If enabled, the theme will be changed to the theme of another user specified in the route parameters if path is in exclude paths.'),
      '#default_value' => $config->get('enable_route_theme'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $this->configFactory->getEditable(self::SETTINGS_NAME)
      ->set('exclude_paths', $values['exclude_paths'] ? explode(PHP_EOL, $values['exclude_paths']) : [])
      ->set('enable_route_theme', $values['enable_theme_by_user_from_route'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
