<?php

namespace Drupal\tsbu\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\extension_reference_field\Enum\ExtensionStatus;
use Drupal\extension_reference_field\Enum\ExtensionType;

/**
 * All users select theme form.
 */
class AllUsersSelectThemeSettingsForm extends ConfigFormBase {

  /**
   * Config name.
   */
  const SETTINGS_NAME = 'tsbu.all_users_select_theme.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tsbu_all_users_select_theme_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS_NAME);
    $themes = ExtensionType::THEME->getExtensionList(ExtensionStatus::ENABLED->value);
    $options = [];
    foreach ($themes as $key => $theme) {
      $options[$key] = $theme->info['name'] ?? $key;
    }
    $form['themes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Themes'),
      '#options' => $options,
      '#default_value' => $config->get('themes') ?? [],
      '#description' => $this->t('Select themes that will be available for all users.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $themes = $form_state->getValue('themes');
    $this->config(self::SETTINGS_NAME)
      ->set('themes', $themes)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
