<?php

namespace Drupal\tsbu;

/**
 * Provides a list controller for the select theme entity type.
 */
class SelectThemeListBuilder extends ThemeSwitcherEntityListBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total select themes: @total', ['@total' => $total]);
    return $build;
  }

}
