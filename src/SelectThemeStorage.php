<?php

namespace Drupal\tsbu;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Select theme storage.
 */
class SelectThemeStorage extends SqlContentEntityStorage implements SelectThemeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByUser(AccountInterface $user): ?SelectThemeInterface {
    $select_theme = $this->loadByProperties([
      'uid' => $user->id(),
    ]);
    if (empty($select_theme)) {
      return NULL;
    }
    return reset($select_theme);
  }

  /**
   * {@inheritdoc}
   */
  public function existsByUser(int $uid): bool {
    $count = $this->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $uid)
      ->count()
      ->execute();
    return $count > 0;
  }

}
