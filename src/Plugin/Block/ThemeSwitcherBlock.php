<?php

namespace Drupal\tsbu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\tsbu\Form\ThemeSwitcherForm;
use Drupal\tsbu\SelectThemeStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Select theme block.
 *
 * @Block(
 *   id = "tsbu_select_theme_block",
 *   admin_label = @Translation("Theme switcher Block"),
 *   category = @Translation("Theme switcher by user")
 * )
 */
class ThemeSwitcherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * Select theme storage.
   *
   * @var \Drupal\tsbu\SelectThemeStorageInterface
   */
  protected SelectThemeStorageInterface $selectThemeStorage;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected ThemeHandlerInterface $themeHandler;

  /**
   * Constructor for SelectThemeField object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SelectThemeStorageInterface $select_theme_storage, AccountProxyInterface $current_user, ThemeHandlerInterface $theme_handler, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->selectThemeStorage = $select_theme_storage;
    $this->currentUser = $current_user;
    $this->themeHandler = $theme_handler;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager')->getStorage('select_theme'),
      $container->get('current_user'),
      $container->get('theme_handler'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $select_theme_form = $this->formBuilder->getForm(ThemeSwitcherForm::class);
    return [
      '#theme' => 'tsbu_theme_switcher_block',
      '#theme_switcher_form' => $select_theme_form,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), [
      'user',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), [
      'select_theme:uid:' . $this->currentUser->id(),
      'available_theme_list',
    ]);
  }

}
