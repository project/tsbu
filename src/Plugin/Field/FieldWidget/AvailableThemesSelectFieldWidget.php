<?php

namespace Drupal\tsbu\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\extension_reference_field\Enum\ExtensionType;
use Drupal\extension_reference_field\Plugin\Field\FieldWidget\ExtensionReferenceSelectWidget;
use Drupal\tsbu\AvailableThemeStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'extension_reference_select' widget.
 *
 * @FieldWidget(
 *   id = "tsbu_available_themes_select",
 *   label = @Translation("Available themes select"),
 *   field_types = {
 *     "extension_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class AvailableThemesSelectFieldWidget extends ExtensionReferenceSelectWidget {

  /**
   * Available themes storage.
   *
   * @var \Drupal\tsbu\AvailableThemeStorageInterface
   */
  protected AvailableThemeStorageInterface $availableThemeStorage;

  /**
   * AvailableThemesSelectFieldWidget constructor.
   *
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   * @param array $settings
   *   Settings.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\tsbu\AvailableThemeStorageInterface $available_theme_storage
   *   Available themes storage.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, AvailableThemeStorageInterface $available_theme_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->availableThemeStorage = $available_theme_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('available_theme')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(FieldableEntityInterface $entity) {
    $options = parent::getOptions($entity);
    $ext_type = $this->fieldDefinition->getSetting('target_type');
    if ($ext_type != ExtensionType::THEME->value) {
      return $options;
    }
    foreach ($options as $theme => $label) {
      if ($this->availableThemeStorage->existsByTheme($theme)) {
        unset($options[$theme]);
      }
    }
    return $options;
  }

}
