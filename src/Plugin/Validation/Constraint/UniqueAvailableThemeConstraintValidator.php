<?php

namespace Drupal\tsbu\Plugin\Validation\Constraint;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\tsbu\AvailableThemeStorageInterface;
use Drupal\tsbu\Form\AllUsersSelectThemeSettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validator for the UniqueAvailableThemeConstraint constraint.
 */
class UniqueAvailableThemeConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Available theme storage.
   *
   * @var \Drupal\tsbu\AvailableThemeStorageInterface
   */
  protected AvailableThemeStorageInterface $availableThemeStorage;

  /**
   * Config for all available themes.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructor for UniqueAvailableThemeConstraintValidator object.
   *
   * @param \Drupal\tsbu\AvailableThemeStorageInterface $available_theme_storage
   *   Available theme storage.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   Config for all available themes.
   */
  public function __construct(AvailableThemeStorageInterface $available_theme_storage, ImmutableConfig $config) {
    $this->availableThemeStorage = $available_theme_storage;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('available_theme'),
      $container->get('config.factory')->get(AllUsersSelectThemeSettingsForm::SETTINGS_NAME)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $value, Constraint $constraint) {
    $theme = $value->getTheme();
    $all_users_available_themes = $this->config->get('themes');
    if ($this->availableThemeStorage->existsByThemeAndUser($theme, $value->getOwnerId()) || (isset($all_users_available_themes[$theme]) && $all_users_available_themes[$theme])) {
      $this->context->addViolation($constraint->message, [
        '%theme' => $theme,
        '@user' => $value->getOwner()->getDisplayName(),
      ]);
    }
  }

}
