<?php

namespace Drupal\tsbu\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if user has already selected the theme.
 *
 * @Constraint(
 *   id = "UniqueAvailableThemeConstraint",
 *   label = @Translation("Unique Available Theme Validation", context = "Validation"),
 *   type = "string"
 * )
 */
class UniqueAvailableThemeConstraint extends Constraint {

  /**
   * The message that will be shown if the user has already selected the theme.
   *
   * @var string
   */
  public $message = 'The theme %theme is already available for @user.';

}
