<?php

namespace Drupal\tsbu;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\extension_reference_field\Enum\ExtensionStatus;

/**
 * Trait for theme field.
 */
trait EntityThemeTrait {

  /**
   * Field definition for theme field.
   */
  public static function themeBaseFieldDefinitions() {
    $fields['theme'] = BaseFieldDefinition::create('extension_reference')
      ->setLabel(t('Theme'))
      ->setDescription(t('The available theme for user'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'theme')
      ->setSetting('status', ExtensionStatus::ENABLED->value)
      ->setDisplayOptions('view', [
        'label' => 'extension_reference_label',
        'type' => 'string',
        'weight' => -6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'extension_reference_select',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    return $fields;
  }

  /**
   * Get value for theme field.
   *
   * @return string|null
   *   Theme id.
   */
  public function getTheme(): ?string {
    return $this->get('theme')->target_id;
  }

  /**
   * Set value for theme field.
   *
   * @param string $theme
   *   Theme id.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function setTheme(string $theme): EntityInterface {
    return $this->set('theme', $theme);
  }

}
