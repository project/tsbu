<?php

namespace Drupal\tsbu;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Available theme storage.
 */
class AvailableThemeStorage extends SqlContentEntityStorage implements AvailableThemeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getAvailableThemesByUser(AccountInterface $user): array {
    return $this->loadByProperties([
      'uid' => $user->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function existsByTheme(string $theme): bool {
    $count = $this->getQuery()
      ->accessCheck(FALSE)
      ->condition('theme', $theme)
      ->count()
      ->execute();
    return $count > 0;
  }

  /**
   * {@inheritdoc}
   */
  public function existsByThemeAndUser(string $theme, int $uid): bool {
    $count = $this->getQuery()
      ->accessCheck(FALSE)
      ->condition('theme', $theme)
      ->condition('uid', $uid)
      ->count()
      ->execute();
    return $count > 0;
  }

}
