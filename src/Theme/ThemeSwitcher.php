<?php

namespace Drupal\tsbu\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\tsbu\Form\ThemeSwitcherSettingsForm;
use Drupal\tsbu\SelectThemeStorageInterface;

/**
 * Theme switcher by user.
 */
class ThemeSwitcher implements ThemeNegotiatorInterface {

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Select theme storage.
   *
   * @var \Drupal\tsbu\SelectThemeStorageInterface
   */
  protected SelectThemeStorageInterface $selectThemeStorage;

  /**
   * Path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected PathMatcherInterface $pathMatcher;

  /**
   * View theme config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $viewThemeConfig;

  /**
   * Current path stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPathStack;

  /**
   * The constructor for ThemeSwitcher object.
   */
  public function __construct(AccountProxyInterface $current_user, EntityTypeManagerInterface $entity_type_manager, PathMatcherInterface $path_matcher, ConfigFactoryInterface $config_factory, CurrentPathStack $current_path_stack) {
    $this->currentUser = $current_user;
    $this->selectThemeStorage = $entity_type_manager->getStorage('select_theme');
    $this->pathMatcher = $path_matcher;
    $this->viewThemeConfig = $config_factory->get(ThemeSwitcherSettingsForm::SETTINGS_NAME);
    $this->currentPathStack = $current_path_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $enable_theme_by_route_user = $this->viewThemeConfig->get('enable_route_theme');
    if (($enable_theme_by_route_user && $route_match->getParameter('user')) || $this->selectThemeStorage->existsByUser($this->currentUser->id())) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $exclude_paths = $this->viewThemeConfig->get('exclude_paths');
    $enable_theme_by_route_user = $this->viewThemeConfig->get('enable_route_theme');
    $path = $this->currentPathStack->getPath();
    if (!$exclude_paths) {
      return $this->selectThemeStorage->loadByUser($this->currentUser)?->getTheme();
    }
    foreach ($exclude_paths as $exclude_path) {
      if ($this->pathMatcher->matchPath($path, $exclude_path)) {
        if ($enable_theme_by_route_user) {
          $user = $route_match->getParameter('user');
          if ($user) {
            return $this->selectThemeStorage->loadByUser($user)?->getTheme();
          }
          else {
            return NULL;
          }
        }
        return NULL;
      }
    }
    return $this->selectThemeStorage->loadByUser($this->currentUser)?->getTheme();
  }

}
