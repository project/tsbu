<?php

namespace Drupal\tsbu;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a select theme entity type.
 */
interface SelectThemeInterface extends ContentEntityInterface, EntityOwnerInterface {

}
